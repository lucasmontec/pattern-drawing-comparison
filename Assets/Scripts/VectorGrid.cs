using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

using static Unity.Mathematics.math;
using float3 = Unity.Mathematics.float3;

public class VectorGrid
{
    private readonly Dictionary<int3, List<float3>> grid = new Dictionary<int3, List<float3>>();

    private readonly float gridDivision;

    private int3 min;
    private int3 max;
    
    private int3 maxAbsGridSize;

    public int Bags => grid.Count;
    
    public int3 Min
    {
        get => min; 
        private set => min = value; 
    }
    
    public int3 Max
    {
        get => max; 
        private set => max = value; 
    }

    public VectorGrid(float gridDivision = 2f)
    {
        if (gridDivision <= 0f)
        {
            throw new Exception("Division must be above 0!");
        }
        this.gridDivision = gridDivision;

        max = int.MinValue;
        min = int.MaxValue;
    }

    public float3 FindNearest(float3 pos)
    {
        if(!grid.Any()) 
            throw new Exception("The grid is empty!");
        
        var set = FindNearestSet(pos);

        float minDist = float.MaxValue;
        float3 minPoint = float3.zero;
        foreach (var point in set)
        {
            var dist = lengthsq(point - pos);

            if (!(dist < minDist)) continue;
            minPoint = point;
            minDist = dist;
        }

        return minPoint;
    }

    public IEnumerable<float3> FindNearestSet(float3 pos)
    {
        if(!grid.Any()) 
            throw new Exception("The grid is empty!");
        
        int3 gridPos = (int3)(pos / gridDivision);
        
        HashSet<float3> checkedPos = new HashSet<float3>();

        int3 currentBoxSize = 0;

        List<float3> nearestSet = new List<float3>();

        bool maxedX = false;
        bool maxedY = false;
        bool maxedZ = false;

        //Calculate the distance from the gridPoint to the grid
        var distToGridMin = distancesq(gridPos, min);
        var distToGridMax = distancesq(gridPos, max);
        var distToGrid = distToGridMin > distToGridMax ? distToGridMin : distToGridMax;

        bool lastRun = false;

        while (!(maxedX && maxedY && maxedZ))
        {
            for (int z = gridPos.z - currentBoxSize.z; z <= gridPos.z + currentBoxSize.z; z++)
            {
                for (int y = gridPos.y - currentBoxSize.y; y <= gridPos.y + currentBoxSize.y; y++)
                {
                    for (int x = gridPos.x - currentBoxSize.x; x <= gridPos.x + currentBoxSize.x; x++)
                    {
                        var currentPos = int3(x, y, z);

                        if (checkedPos.Contains(currentPos)) continue;
                        checkedPos.Add(currentPos);
                        
                        if (!grid.ContainsKey(currentPos)) continue;

                        var points = grid[currentPos];
                        nearestSet.AddRange(points);
                    }
                }
            }

            if (lastRun)
            {
                break;
            }
            
            if (nearestSet.Count != 0)
            {
                lastRun = true;
            }

            //Increase box size
            if (currentBoxSize.x < distToGrid*2)
            {
                currentBoxSize.x++;
            }
            else
            {
                maxedX = true;
            }
            
            if (currentBoxSize.y < distToGrid*2)
            {
                currentBoxSize.y++;
            }
            else
            {
                maxedY = true;
            }
            
            if (currentBoxSize.z < distToGrid*2)
            {
                currentBoxSize.z++;
            }
            else
            {
                maxedZ = true;
            }
        }

        return nearestSet;
    }

    /// <summary>
    /// Returns an vector with the absolute biggest values on min and max
    /// for each axis for the grid.
    /// </summary>
    /// <returns></returns>
    private int3 AbsMaxGrid()
    {
        int3 maxAbs = abs(max);
        int3 minAbs = abs(min);

        int3 result;

        result.x = maxAbs.x > minAbs.x ? maxAbs.x : minAbs.x;
        result.y = maxAbs.y > minAbs.y ? maxAbs.y : minAbs.y;
        result.z = maxAbs.z > minAbs.z ? maxAbs.z : minAbs.z;

        return result;
    }
    
    public void AddToGrid(float3 pos)
    {
        int3 gridPos = (int3)(pos / gridDivision);

        SaveMax(gridPos);
        SaveMin(gridPos);
        maxAbsGridSize = AbsMaxGrid();

        List<float3> bag;
        
        if (grid.ContainsKey(gridPos))
        {
            bag = grid[gridPos];
        }
        else
        {
            bag = new List<float3>();
            grid[gridPos] = bag;
        }

        bag.Add(pos);
    }

    private void SaveMax(int3 gridPos)
    {
        if (gridPos.x > max.x)
        {
            max.x = gridPos.x;
        }

        if (gridPos.y > max.y)
        {
            max.y = gridPos.y;
        }

        if (gridPos.z > max.z)
        {
            max.z = gridPos.z;
        }
    }
    
    private void SaveMin(int3 gridPos)
    {
        if (gridPos.x < min.x)
        {
            min.x = gridPos.x;
        }

        if (gridPos.y < min.y)
        {
            min.y = gridPos.y;
        }

        if (gridPos.z < min.z)
        {
            min.z = gridPos.z;
        }
    }

    public void AddToGrid(IEnumerable<Vector3> pos)
    {
        foreach (var po in pos)
        {
            AddToGrid(po);
        }
    }
}