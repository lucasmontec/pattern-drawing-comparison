using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class AngleTest : MonoBehaviour
{

    public Vector3 a;
    public Vector3 b;
    public Vector3 c;
    public Vector3 d;

    [ReadOnly]
    public float result;

    [ReadOnly]
    public List<float> listAnglesResult;
    
    [Button]
    public void Compute()
    {
        result = MathUtil.AngleBetween3Points(a,b,c);
    }
    
    [Button]
    public void ComputeList()
    {
        List<Vector3> vecs = new List<Vector3> {a,b,c,d};
        listAnglesResult = MathUtil.AnglesFromVectorList(vecs);
    }
}