﻿using System;
using System.Collections;

public class SimpleDtw
{
    private readonly float[] x;
    private readonly float[] y;
    private readonly float[,] distance;
    private readonly float[,] f;
    private readonly ArrayList pathX;
    private readonly ArrayList pathY;
    private readonly ArrayList distanceList;
    private float sum;

    public SimpleDtw(float[] x, float[] y)
    {
        this.x = x;
        this.y = y;
        distance = new float[this.x.Length, this.y.Length];
        f = new float[this.x.Length + 1, this.y.Length + 1];

        for (int i = 0; i < this.x.Length; ++i)
        {
            for (int j = 0; j < this.y.Length; ++j)
            {
                distance[i, j] = Math.Abs(this.x[i] - this.y[j]);
            }
        }

        for (int i = 0; i <= this.x.Length; ++i)
        {
            for (int j = 0; j <= this.y.Length; ++j)
            {
                f[i, j] = -1.0f;
            }
        }

        for (int i = 1; i <= this.x.Length; ++i)
        {
            f[i, 0] = float.PositiveInfinity;
        }

        for (int j = 1; j <= this.y.Length; ++j)
        {
            f[0, j] = float.PositiveInfinity;
        }

        f[0, 0] = 0f;
        sum = 0f;

        pathX = new ArrayList();
        pathY = new ArrayList();
        distanceList = new ArrayList();
    }

    public ArrayList GetPathX()
    {
        return pathX;
    }

    public ArrayList GetPathY()
    {
        return pathY;
    }

    public float GetSum()
    {
        return sum;
    }

    public float[,] GetFMatrix()
    {
        return f;
    }

    public ArrayList GetDistanceList()
    {
        return distanceList;
    }

    public void ComputeDtw()
    {
        sum = ComputeFBackward(x.Length, y.Length);
        //sum = computeFForward();
    }

    public float ComputeFForward()
    {
        for (int i = 1; i <= x.Length; ++i)
        {
            for (int j = 1; j <= y.Length; ++j)
            {
                if (f[i - 1, j] <= f[i - 1, j - 1] && f[i - 1, j] <= f[i, j - 1])
                {
                    f[i, j] = distance[i - 1, j - 1] + f[i - 1, j];
                }
                else if (f[i, j - 1] <= f[i - 1, j - 1] && f[i, j - 1] <= f[i - 1, j])
                {
                    f[i, j] = distance[i - 1, j - 1] + f[i, j - 1];
                }
                else if (f[i - 1, j - 1] <= f[i, j - 1] && f[i - 1, j - 1] <= f[i - 1, j])
                {
                    f[i, j] = distance[i - 1, j - 1] + f[i - 1, j - 1];
                }
            }
        }

        return f[x.Length, y.Length];
    }

    public float ComputeFBackward(int i, int j)
    {
        if (!(f[i, j] < 0.0))
        {
            return f[i, j];
        }

        if (ComputeFBackward(i - 1, j) <= ComputeFBackward(i, j - 1) && ComputeFBackward(i - 1, j) <=
                                                                     ComputeFBackward(i - 1, j - 1)
                                                                     && ComputeFBackward(i - 1, j) <
                                                                     float.PositiveInfinity)
        {
            f[i, j] = distance[i - 1, j - 1] + ComputeFBackward(i - 1, j);
        }
        else if (ComputeFBackward(i, j - 1) <= ComputeFBackward(i - 1, j) && ComputeFBackward(i, j - 1) <=
                                                                          ComputeFBackward(i - 1, j - 1)
                                                                          && ComputeFBackward(i, j - 1) <
                                                                          float.PositiveInfinity)
        {
            f[i, j] = distance[i - 1, j - 1] + ComputeFBackward(i, j - 1);
        }
        else if (ComputeFBackward(i - 1, j - 1) <= ComputeFBackward(i - 1, j) && ComputeFBackward(i - 1, j - 1) <=
                                                                              ComputeFBackward(i, j - 1)
                                                                              && ComputeFBackward(i - 1, j - 1) <
                                                                              float.PositiveInfinity)
        {
            f[i, j] = distance[i - 1, j - 1] + ComputeFBackward(i - 1, j - 1);
        }

        return f[i, j];
    }
}