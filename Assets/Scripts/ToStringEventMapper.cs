﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;

public class ToStringEventMapper : MonoBehaviour
{
    [Serializable]
    public class StringEvent : UnityEvent<string> {}

    public StringEvent onValue;

    public void OnFloatValue(float v)
    {
        onValue?.Invoke(v.ToString(CultureInfo.InvariantCulture));
    }
} 
