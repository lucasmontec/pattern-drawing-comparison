using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class LineDrawDtw : SerializedMonoBehaviour
{
    [Required]
    public LineDraw lineA;

    [Required]
    public LineDraw lineB;

    public Vector3Dtw.DtwTrimMethod trimMethod;

    public Vector3Dtw.ProportionMethod proportionMethod;
    
    [Serializable]
    public class FloatEvent : UnityEvent<float> {}

    public FloatEvent onResult;
    
    [Button]
    public void Compute()
    {
        float distance = Vector3Dtw.Compute(lineA.Points, lineB.Points, trimMethod, proportionMethod);
        float distance2 = Vector3Dtw.Compute(lineA.Points.Reverse(), lineB.Points, trimMethod, proportionMethod);
        onResult?.Invoke(Mathf.Min(distance, distance2));
    }
}