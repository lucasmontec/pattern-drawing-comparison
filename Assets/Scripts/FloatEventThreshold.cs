﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class FloatEventThreshold : MonoBehaviour
{
    [Serializable]
    public class FloatEvent : UnityEvent<float> {}

    public FloatEvent onValueAbove;

    public FloatEvent onValueBellow;
    
    public float threshold;
    
    public void OnFloatValue(float v)
    {
        if (v > threshold)
        {
            onValueAbove?.Invoke(v);
        }
        
        if (v < threshold)
        {
            onValueBellow?.Invoke(v);
        }
    }
}
