using System.Collections.Generic;
using UnityEngine;

public class Bounds
{
    private readonly Vector3 min;
    private readonly Vector3 max;

    public Bounds(Vector3 min, Vector3 max)
    {
        this.min = min;
        this.max = max;
    }

    public Vector3 Center => (min + max) * 0.5f;

    public Vector3 Size => max - min;
    
    public static Bounds FromPoints(List<Vector3> points)
    {
        Vector3 min = points[0];
        Vector3 max = points[0];

        foreach (var point in points)
        {
            if (point.x < min.x)
            {
                min.x = point.x;
            }
            if (point.x > max.x)
            {
                max.x = point.x;
            }
            
            if (point.y < min.y)
            {
                min.y = point.y;
            }
            if (point.y > max.y)
            {
                max.y = point.y;
            }
            
            if (point.y < min.y)
            {
                min.y = point.y;
            }
            if (point.y > max.y)
            {
                max.y = point.y;
            }
        }

        return new Bounds(min, max);
    }

    public static float VolumeProportion(Bounds a, Bounds b)
    {
        return b.Volume() / a.Volume();
    }
    
    public static float VolumeProportionSafe(Bounds a, Bounds b)
    {
        return b.VolumeSafe() / a.VolumeSafe();
    }
    
    public static float PerimeterProportion(Bounds a, Bounds b)
    {
        return b.Perimeter() / a.Perimeter();
    }

    public static Vector3 ProportionalVector(Bounds a, Bounds b)
    {
        var xProp = b.Size.x / a.Size.x;
        var yProp = b.Size.y / a.Size.y;
        var zProp = b.Size.z / a.Size.z;

        if (float.IsNaN(xProp))
        {
            xProp = 0;
        }
        if (float.IsNaN(yProp))
        {
            yProp = 0;
        }
        if (float.IsNaN(zProp))
        {
            zProp = 0;
        }
        
        return new Vector3(xProp, yProp, zProp);
    }
    
    public float Perimeter()
    {
        float sizeX = Mathf.Max(1,max.x - min.x);
        float sizeY = Mathf.Max(1,max.y - min.y);
        float sizeZ = Mathf.Max(1,max.z - min.z);

        return 4*(sizeX + sizeY + sizeZ);
    }
    
    public float VolumeSafe()
    {
        float sizeX = Mathf.Max(1,max.x - min.x);
        float sizeY = Mathf.Max(1,max.y - min.y);
        float sizeZ = Mathf.Max(1,max.z - min.z);

        return sizeX * sizeY * sizeZ;
    }
    
    public float Volume()
    {
        float sizeX = max.x - min.x;
        float sizeY = max.y - min.y;
        float sizeZ = max.z - min.z;

        return sizeX * sizeY * sizeZ;
    }
}