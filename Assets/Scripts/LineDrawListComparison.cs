using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

public class LineDrawListComparison : SerializedMonoBehaviour
{
    [Required]
    public LineDraw lineA;

    [Required]
    public LineDraw lineB;

    public Vector3ListComparison.TrimMethod trimMethod;

    public Vector3ListComparison.ProportionMethod proportionMethod;
    
    [Serializable]
    public class FloatEvent : UnityEvent<float> {}

    public FloatEvent onResult;

    [BoxGroup("Approximation")]
    public float percent = 0.5f;
    
    [BoxGroup("Approximation")]
    public int approximationRounds = 1;
    
    [BoxGroup("Approximation")]
    public bool useApproximation;
    
    private List<Vector3> debugA;
    private List<Vector3> debugB;

    private Bounds boundsA;
    private Bounds boundsB;

    [Button]
    public void ExpoApprox()
    {
        if(debugA == null || debugB == null) return;

        debugA = MathUtil.InterpolationApproximation(debugA, debugB, MathUtil.EaseInQuad, percent, 0.2f);
        debugB = MathUtil.InterpolationApproximation(debugB, debugA, MathUtil.EaseInQuad, percent, 0.2f);
    }    
    
    [Button]
    public void LerpApprox()
    {
        if(debugA == null || debugB == null) return;

        debugA = MathUtil.InterpolationApproximation(debugA, debugB, Vector3.Lerp, percent);
    }

    [Button]
    public void Debug()
    {
        debugA = new List<Vector3>(lineA.Points);
        debugB = new List<Vector3>(lineB.Points);

        //Get outlier direction change angle avg from shapes
        //smoother shapes like circles will have 0 or near zero 
        //pointy shapes will have an angle 
        //Use this outlier-pointy angle to select a good simplification angle for the shape
        //This simplification angle in theory should respect a bit more the shape properties.
        //This could be done maybe in a rolling avg because shapes can have straight regions and
        //smooth regions. We want to preserve some smoothness and to straight relatively straight zones.
        
        //Simplify by outliers using mean

        Vector3ListComparison.NormalizeAndStandardize(ref debugA, ref debugB, proportionMethod);
        
        debugA = MathUtil.SimplifyShapeAuto(debugA, 0.2f);
        debugB = MathUtil.SimplifyShapeAuto(debugB, 0.2f);
        
        /*
        debugA = MathUtil.SimplifyShapeAuto(debugA, 0.8f);
        debugB = MathUtil.SimplifyShapeAuto(debugB, 0.8f);
        
        debugA = MathUtil.SimplifyNear(debugA, 0.33f);
        debugB = MathUtil.SimplifyNear(debugB, 0.33f);
        */
        
        boundsA = Bounds.FromPoints(debugA);
        boundsB = Bounds.FromPoints(debugB);
    }
    
    [Button]
    public void Compute()
    {
        if(lineA.Points.Count < 3) return;
        if(lineB.Points.Count < 3) return;

        var aPoints = new List<Vector3>(lineA.Points);
        
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        
        if (useApproximation)
        {
            aPoints = MathUtil.InterpolationApproximation(lineA.Points, lineB.Points, MathUtil.EaseInQuad, percent, 1.5f);
            
            print($"approx overall {stopwatch.ElapsedMilliseconds}");
            stopwatch.Restart();
        }

        float distance = Vector3ListComparison.Compute(aPoints, lineB.Points, 1f, trimMethod, proportionMethod);

        print($"compute overall {stopwatch.ElapsedMilliseconds}");
        stopwatch.Restart();
        
        //VERTEX COMPARISON
        var simplifiedA = MathUtil.SimplifyShapeAuto(lineA.Points);
        var simplifiedB = MathUtil.SimplifyShapeAuto(lineB.Points);

        simplifiedA = MathUtil.SimplifyShapeAuto(simplifiedA, 0.8f);
        simplifiedB = MathUtil.SimplifyShapeAuto(simplifiedB, 0.8f);

        simplifiedA = MathUtil.SimplifyNear(simplifiedA, 0.33f);
        simplifiedB = MathUtil.SimplifyNear(simplifiedB, 0.33f);
        
        print($"Simplify {stopwatch.ElapsedMilliseconds}");

        float vertexDiff = Math.Abs(simplifiedA.Count - simplifiedB.Count);
        print($"vertex diff: {vertexDiff}");
        vertexDiff *= 0.02f;

        //Simplified distances
        var sA = MathUtil.SimplifyShapeAuto(lineA.Points, 0.2f);
        var sB = MathUtil.SimplifyShapeAuto(lineB.Points, 0.2f);
        
        if (useApproximation)
        {
            stopwatch.Restart();
            for (int i = 0; i < approximationRounds; i++)
            {
                sA = MathUtil.InterpolationApproximation(sA, sB, MathUtil.EaseInQuad, percent, 1f);
            }
            for (int i = 0; i < approximationRounds; i++)
            {
                sB = MathUtil.InterpolationApproximation(sB, sA, MathUtil.EaseInQuad, percent, 1f);
            }
            print($"Simplified approx {stopwatch.ElapsedMilliseconds}");
            stopwatch.Restart();
        }

        float avgAngleChangesA = MathUtil.OutlierAngleAverage(sA, 0.6f);
        float avgAngleChangesB = MathUtil.OutlierAngleAverage(sB, 0.6f);
        print($"Outlier avg diff: {Mathf.Abs(avgAngleChangesA - avgAngleChangesB)}");
        
        float simplifiedAToBDistance = Vector3ListComparison.Compute(sA, sB, 0.2f, trimMethod, proportionMethod);
        float simplifiedBToADistance = Vector3ListComparison.Compute(sB, sA, 0.2f, trimMethod, proportionMethod);

        print($"Simplified distance pair {stopwatch.ElapsedMilliseconds}");
        stopwatch.Restart();
        
        float maxSimplifiedDistance = Mathf.Max(simplifiedAToBDistance, simplifiedBToADistance);
        
        float overallDistance = maxSimplifiedDistance * 0.3f + distance * 0.5f + vertexDiff * 0.2f;
        
        onResult?.Invoke(overallDistance);
    }

    private void OnDrawGizmos()
    {
        if(debugA == null) return;
        if(debugB == null) return;

        var position = transform.position;
        Gizmos.color = Color.blue;
        
        for (var index = 0; index < debugA.Count-1; index++)
        {
            var point = debugA[index];
            var nextPoint = debugA[index+1];

            point *= -1;
            nextPoint *= -1;
            
            Gizmos.DrawLine(position+point, position+nextPoint);
        }

        //Gizmos.DrawWireCube(boundsA.Center, boundsA.Size);
        
        /*
        var lastPointPos = debugA[debugA.Count-1];
        Gizmos.DrawLine(position+lastPointPos, position+debugA[0]);
        */
        
        Gizmos.color = Color.red;
        
        for (var index = 0; index < debugB.Count-1; index++)
        {
            var point = debugB[index];
            var nextPoint = debugB[index+1];
            
            point *= -1;
            nextPoint *= -1;
            
            Gizmos.DrawLine(position+point, position+nextPoint);
        }

        //Gizmos.DrawWireCube(boundsB.Center, boundsB.Size);
        
        /*
        lastPointPos = debugB[debugB.Count-1];
        Gizmos.DrawLine(position+lastPointPos, position+debugB[0]);
        */
    }
}