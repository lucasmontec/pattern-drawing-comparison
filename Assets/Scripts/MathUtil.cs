using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class MathUtil
{
    
    /// <summary>
    /// Calculates the angle formed on b given 3 consecutive points a,b,c.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    /// <returns></returns>
    public static float AngleBetween3Points(Vector3 a, Vector3 b, Vector3 c)
    {
        var bToA = a-b;
        var bToC = c - b;
        return AngleSigned(bToA, bToC, Vector3.Cross(bToA, bToC));
    }
    
    public static float AngleBetween3Points(Vector3 a, Vector3 b, Vector3 c, Vector3 n)
    {
        var bToA = a-b;
        var bToC = c - b;
        return AngleSigned(bToA, bToC, b);
    }
    
    public static List<float> AnglesFromVectorList(IReadOnlyList<Vector3> vectors, Vector3 n)
    {
        List<float> angles = new List<float>();
        for (int i = 0; i < vectors.Count-2; i++)
        {
            var a = vectors[i];
            var b = vectors[i+1];
            var c = vectors[i+2];

            angles.Add(AngleBetween3Points(a,b,c,n));
        }

        return angles;
    }
    
    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    /// <summary>
    /// Takes every 3 vectors and calculates the angle with the middle vector as the
    /// vertex for the angle.
    /// </summary>
    /// <returns></returns>
    public static List<float> AnglesFromVectorList(IReadOnlyList<Vector3> vectors)
    {
        List<float> angles = new List<float>();
        for (int i = 0; i < vectors.Count-2; i++)
        {
            var a = vectors[i];
            var b = vectors[i+1];
            var c = vectors[i+2];

            angles.Add(AngleBetween3Points(a,b,c));
        }

        return angles;
    }

    public static int CountOutliers(List<float> floats, float avgMultLimiter = 4f)
    {
        var avg = floats.Sum() / floats.Count;
        return floats.Count(f => f > avg * avgMultLimiter);
    }
    
    public static float SumOutliers(List<float> floats, float avgMultLimiter = 4f)
    {
        var avg = floats.Sum() / floats.Count;
        return floats.Where(f => f > avg * avgMultLimiter).Sum();
    }
    
    public static float StdDev(List<float> floats)
    {
        return Mathf.Sqrt(Variance(floats));
    }

    public static float Variance(List<float> floats)
    {
        var avg = floats.Sum() / floats.Count;
        float accum = floats.Sum(f => (f - avg) * (f - avg));
        return accum / floats.Count;
    }

    public static Vector3 EaseInCubic(Vector3 start, Vector3 end, float value)
    {
        end -= start;
        return end * value * value * value + start;
    }
    
    public static Vector3 EaseInQuad(Vector3 start, Vector3 end, float value)
    {
        end -= start;
        return end * value * value + start;
    }
    
    public static Vector3 EaseInQuart(Vector3 start, Vector3 end, float value)
    {
        end -= start;
        return end * value * value * value * value + start;
    }
    
    public static float EaseInExpo(float start, float end, float value)
    {
        end -= start;
        return end * Mathf.Pow(2, 10 * (value - 1)) + start;
    }
    
    public static Vector3 EaseInExpo(Vector3 start, Vector3 end, float value)
    {
        end -= start;
        return end * Mathf.Pow(2, 10 * (value - 1)) + start;
    }


    /// <summary>
    /// Approximate each point on the source list to the target list by applying a interpolator to them.
    /// </summary>
    /// <param name="source">The source list to approximate to nearest points on the target list.</param>
    /// <param name="target">The target list.</param>
    /// <param name="interpolator">The interpolator function to use for approximation.</param>
    /// <param name="approxPercent">How much to approximate using the interpolator function (0-1) percentage.</param>
    /// <param name="gridSize"></param>
    /// <returns></returns>
    public static List<Vector3> InterpolationApproximation(
        IReadOnlyList<Vector3> source,
        IEnumerable<Vector3> target, 
        Func<Vector3, Vector3, float, Vector3> interpolator,
        float approxPercent=0.2f,
        float gridSize=0.5f)
    {
        List<Vector3> approximated = new List<Vector3>();

        //Build grid for target
        var targetGrid = new VectorGrid(gridSize);
        targetGrid.AddToGrid(target);

        for (var sourceIndex = 0; sourceIndex < source.Count; sourceIndex++)
        {
            var sourceVec = source[sourceIndex];
            //Find nearest in target
            Vector3 nearest = targetGrid.FindNearest(sourceVec);
            
            sourceVec = interpolator(sourceVec, nearest, approxPercent);

            approximated.Insert(sourceIndex, sourceVec);
        }

        return approximated;
    }
    
    /// <summary>
    /// Approximate each point on the source list to the target list by applying gravity to them.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="targetMass"></param>
    /// <param name="iterations"></param>
    /// <returns></returns>
    public static List<Vector3> GravityApproximation(IReadOnlyList<Vector3> source, IReadOnlyList<Vector3> target, float targetMass=1, int iterations = 2)
    {
        List<Vector3> approximated = new List<Vector3>();
        List<Vector3> velocities = new List<Vector3>();

        const float g = 0.0667f;
        
        //Build grid for target
        var targetGrid = new VectorGrid();
        targetGrid.AddToGrid(target);

        for (var sourceIndex = 0; sourceIndex < source.Count; sourceIndex++)
        {
            var sourceVec = source[sourceIndex];
            //Find nearest in target
            Vector3 nearest = targetGrid.FindNearest(sourceVec);
            
            var firstDistance = Vector3.Distance(sourceVec, nearest);
            
            Debug.DrawLine(-sourceVec, -nearest, Color.green, 15f);
            
            for (int i = 0; i < iterations; i++)
            {
                //Get current vel
                var currentVel = velocities.ElementAtOrDefault(sourceIndex);
                
                //Move point to nearest
                var r = Vector3.Distance(sourceVec, nearest);
                var gravityPull = (g * targetMass) / (r * r);
                gravityPull = Mathf.Min(gravityPull, firstDistance*5f);
                var dir = (nearest - sourceVec).normalized;
                currentVel += dir * gravityPull * 0.02f;
                
                //Move
                sourceVec += currentVel * 0.02f;

                velocities.Insert(sourceIndex, currentVel);
            }

            approximated.Insert(sourceIndex, sourceVec);
        }

        return approximated;
    }
    
    /// <summary>
    /// Simplifies vertices that are near by removing vertex pairs.
    /// Each pair removed is substituted with their intermediate vertex.
    /// A vertex is considered to be near another if the distance between it and its pair is less than the
    /// average length of distances on the list multiplied by percentFromAvgLengths (default 30%).
    ///
    /// Near = Avg(vertices distances) * percentFromAvgLengths
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="percentFromAvgLengths"></param>
    /// <returns></returns>
    public static List<Vector3> SimplifyNear(IReadOnlyList<Vector3> vertices, float percentFromAvgLengths = 0.3f)
    {
        if (vertices.Count < 3) 
            return new List<Vector3>(vertices);
        
        List<Vector3> simplified = new List<Vector3>(vertices);

        //Calc avg length
        float avgLengths = 0;
        for (int i = 0; i < vertices.Count-2; i++)
        {
            var dist = Vector3.Distance(vertices[i], vertices[i + 1]);
            avgLengths += dist;
        }
        avgLengths /= vertices.Count;

        var lengthFactor = avgLengths * percentFromAvgLengths;
        
        //Debug.Log($"original simplified.Count {simplified.Count} lengthFactor {lengthFactor}");
        int currentIndex = 0;
        do
        {
            //Get current
            var current = simplified[currentIndex];
            var next = simplified[currentIndex + 1];

            //Get distance to next
            var distance = Vector3.Distance(current, next);

            //Check if current stays
            if (distance < lengthFactor)
            {
                //Remove current and remove next, substitute with a sum of both
                simplified.RemoveAt(currentIndex);
                simplified.RemoveAt(currentIndex);

                //Add intermediate point
                var intermediate = (current + next) * 0.5f;
                //Debug.Log($"intermediate {intermediate} current {current} next {next}");
                simplified.Insert(currentIndex, intermediate);
                //Debug.Log($"Removed {currentIndex} and {currentIndex+1} and added {intermediate} at {currentIndex} with length {distance}");
            }
            else
            {
                //Debug.Log($"Kept {currentIndex} and {currentIndex+1} with length {distance}");
                currentIndex++;
            }
            
            //Debug.Log($"simplified.Count {simplified.Count}");
        } while (currentIndex < simplified.Count-2);

        return simplified;
    }
    
    public static List<Vector3> SimplifyShapeAuto(IReadOnlyList<Vector3> vertices, float avgOutliersBias = 0.4f)
    {
        var angles = AngleChanges(vertices);
        var avgAngleChanges = angles.Sum() / angles.Count;

        if (!angles.Any())
        {
            return new List<Vector3>(vertices);
        }
        
        var greatest = angles.Max();
        var proportionFromGreatestToAvg = greatest / avgAngleChanges;
        proportionFromGreatestToAvg *= 0.3f;
        
        var outliers = angles.Where(a => a > avgAngleChanges * proportionFromGreatestToAvg);
        var outs = outliers.ToList();
        var avgOutliers = outs.Sum() / outs.Count;

        var outliersBias = avgOutliers*avgOutliersBias;
        return SimplifyLinearShape(vertices, outliersBias);
    }

    public static List<Vector3> SimplifyLinearShape(IReadOnlyList<Vector3> vertices, float maxAngle, bool project=false, bool connect=false)
    {
        if(vertices.Count < 3) 
            return new List<Vector3>(vertices);
        
        List<Vector3> simplifiedList = new List<Vector3>
        {
            vertices[0]
        };

        Vector3 lastDir;
        Vector3 currDir;
        Vector3 jumpDir;
        float angle;
        
        //For each new vector, check the last dir (formed by the last 2 vectors).
        // A -> B -> C 
        // A -> B is the direction we predict the new point C will be in
        // if B -> C has a small angle compared to A -> B, we can remove B from the list, and add
        // Cproj to the list where C proj is C projected in the line formed from a + A->B as a dir.
        // If the angle is big, just add the vector directly

        for(int i = 1; i < vertices.Count - 1; i++)
        {
            lastDir = vertices[i] - simplifiedList[simplifiedList.Count - 1];
            currDir = vertices[i + 1] - vertices[i];

            angle = Vector3.Angle(lastDir, currDir);

            if (angle > maxAngle)
            {
                simplifiedList.Add(vertices[i]);
            }
            else
            {
                if (!project) continue;

                //Add new projected
                jumpDir = vertices[i + 1] - simplifiedList[simplifiedList.Count - 1];
                var nProj = NearestPointOnLine(vertices[i-1], jumpDir, vertices[i]);
                simplifiedList.Add(nProj);
            }
        }

        if (!connect)
        {
            simplifiedList.Add(vertices[vertices.Count - 1]);
            return simplifiedList;
        }
        
        lastDir = simplifiedList[simplifiedList.Count - 1] - vertices[vertices.Count - 2];
        currDir = vertices[1] - vertices[0];
        angle = Vector3.Angle(lastDir, currDir);
            
        if (angle > maxAngle)
        {
            simplifiedList.Add(vertices[0]);
        }
        else
        {
            //Add new projected
            if (!project) return simplifiedList;
            
            jumpDir = vertices[0] - vertices[vertices.Count - 2];
            var nProj = NearestPointOnLine(vertices[simplifiedList.Count - 2], jumpDir, vertices[0]);
            simplifiedList.Add(nProj);
        }
        
        return simplifiedList;
    }

    /// <summary>
    /// Calculates the average of outlier angles in a shape defined by a continuous list of vertices.
    ///
    /// var greatest = angles.Max();
    /// var proportionFromGreatestToAvg = greatest / avgAngleChanges;
    /// proportionFromGreatestToAvg *= proportionBias;
    ///
    /// a > avgAngleChanges * proportionFromGreatestToAvg
    /// </summary>
    /// <param name="vertices">The shape.</param>
    /// <param name="proportionBias">To define an outlier the proportion is defined multiplied by this.</param>
    /// <returns></returns>
    public static float OutlierAngleAverage(IReadOnlyList<Vector3> vertices, float proportionBias=0.3f)
    {
        var angles = AngleChanges(vertices);
        var avgAngleChanges = angles.Sum() / angles.Count;

        if (!angles.Any())
        {
            return 0f;
        }
        
        var greatest = angles.Max();
        var proportionFromGreatestToAvg = greatest / avgAngleChanges;
        proportionFromGreatestToAvg *= proportionBias;
        
        var outliers = angles.Where(a => a > avgAngleChanges * proportionFromGreatestToAvg);
        var outs = outliers.ToList();

        return outs.Average();
    }

    public static List<float> AngleChanges(IReadOnlyList<Vector3> vectors)
    {
        List<float> angleChanges = new List<float>();
        
        if (vectors.Count < 3) return angleChanges;

        Vector3 lastDir = Vector3.zero;

        bool calculatedFirstDir = false;
        
        for (int i = 0; i < vectors.Count-1; i++)
        {
            //Dir is a to b
            var a = vectors[i];
            var b = vectors[i+1];

            var dir = Vector3.Normalize(b - a);

            if (!calculatedFirstDir)
            {
                calculatedFirstDir = true;
                lastDir = dir;
                continue;
            }
            
            float angleBetweenDirections = Vector3.Angle(lastDir, dir);
            angleChanges.Add(angleBetweenDirections);
            lastDir = dir;
        }

        return angleChanges;
    }
    
    /// <summary>
    /// Gets a list of every new direction that changes with an angle of more than maxDevianceAngle deg.
    /// </summary>
    /// <param name="vectors"></param>
    /// <param name="maxDevianceAngle"></param>
    /// <returns></returns>
    public static List<Vector3> DirectionChanges(IReadOnlyList<Vector3> vectors, float maxDevianceAngle = 20f)
    {
        List<Vector3> dirs = new List<Vector3>();
        
        if (vectors.Count < 3) return dirs;

        Vector3 lastDir = Vector3.zero;

        bool calculatedFirstDir = false;
        
        for (int i = 0; i < vectors.Count-1; i++)
        {
            //Dir is a to b
            var a = vectors[i];
            var b = vectors[i+1];

            var dir = Vector3.Normalize(b - a);

            if (!calculatedFirstDir)
            {
                calculatedFirstDir = true;
                lastDir = dir;
                dirs.Add(dir);
                continue;
            }
            
            float angleBetweenDirections = Vector3.Angle(lastDir, dir);

            if (angleBetweenDirections > maxDevianceAngle)
            {
                dirs.Add(dir);
            }

            lastDir = dir;
        }

        return dirs;
    }
    
    public static List<float> DistsToPredictedDirection(IReadOnlyList<Vector3> vectors)
    {
        List<float> dists = new List<float>();
        
        if (vectors.Count < 3) return dists;
        
        for (int i = 0; i < vectors.Count-2; i++)
        {
            //Dir is a to b
            var a = vectors[i];
            var b = vectors[i+1];

            var dir = Vector3.Normalize(b - a);
            
            var point = vectors[i+2];

            //Project P on line
            var pointOnExpectedDir = NearestPointOnLine(a, dir, point);

            var distProjToPoint = Vector3.Distance(point, pointOnExpectedDir);
            dists.Add(distProjToPoint);
        }

        return dists;
    }
    
    public static Vector3 NearestPointOnLine(Vector3 linePnt, Vector3 lineDir, Vector3 pnt)
    {
        lineDir.Normalize();//this needs to be a unit vector
        var v = pnt - linePnt;
        var d = Vector3.Dot(v, lineDir);
        return linePnt + lineDir * d;
    }
}