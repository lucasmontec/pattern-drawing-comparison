using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Vector3ListComparison
{
    public enum ProportionMethod
    {
        BiggestToSmallest,
        SmallestToBiggest,
        None
    }
    
    public enum TrimMethod
    {
        TrimFirst,
        TrimSecond,
        Auto,
        None
    }

    private enum Axis
    {
        X,
        Y,
        Z
    }

    public static void NormalizeAndStandardize(ref List<Vector3> first, ref List<Vector3> second, ProportionMethod proportionFixMethod = ProportionMethod.SmallestToBiggest)
    {
        //Standardize vectors of both lists (align and transform them to offsets from their center of mass)
        first = StandardizeVectors(first);
        second = StandardizeVectors(second);
        
        //Fix proportion between the drawings
        NormalizeProportion(proportionFixMethod, ref first, ref second);
    }
    
    public static float Compute(IReadOnlyList<Vector3> first, IReadOnlyList<Vector3> second, float gridDivision = 2f, TrimMethod trim = TrimMethod.Auto, ProportionMethod proportionFixMethod = ProportionMethod.SmallestToBiggest)
    {
        var listA = new List<Vector3>(first);
        var listB = new List<Vector3>(second);

        //Trim lists
        Trim(first, second, trim, ref listA, ref listB);
        
        //Standardize vectors of both lists (align and transform them to offsets from their center of mass)
        listA = StandardizeVectors(listA);
        listB = StandardizeVectors(listB);

        //Fix proportion between the drawings
        NormalizeProportion(proportionFixMethod, ref listA, ref listB);

        VectorSetMinDistance vecDist = new VectorSetMinDistance();
        float accumulatedDistances = listA
            .Sum(pointInA => vecDist.MinDistance(pointInA, listB, gridDivision));

        return accumulatedDistances / listA.Count;
    }

    private static void Trim(IReadOnlyCollection<Vector3> first, IReadOnlyCollection<Vector3> second, TrimMethod trim, ref List<Vector3> listA, ref List<Vector3> listB)
    {
        switch (trim)
        {
            case TrimMethod.TrimFirst:
                if (first.Count > second.Count)
                {
                    listA = listA.GetRange(0, listB.Count);
                }

                break;
            case TrimMethod.TrimSecond:
                if (second.Count > first.Count)
                {
                    listB = listB.GetRange(0, listA.Count);
                }

                break;
            case TrimMethod.Auto:
                if (listA.Count > listB.Count)
                {
                    listA = listA.GetRange(0, listB.Count);
                }
                else
                {
                    listB = listB.GetRange(0, listA.Count);
                }

                break;
            case TrimMethod.None:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(trim), trim, null);
        }
    }
    
    private static void NormalizeProportion(ProportionMethod proportionFixMethod, ref List<Vector3> listA, ref List<Vector3> listB)
    {
        Bounds aBounds = Bounds.FromPoints(listA);
        Bounds bBounds = Bounds.FromPoints(listB);
        switch (proportionFixMethod)
        {
            case ProportionMethod.BiggestToSmallest:
                if (aBounds.VolumeSafe() > bBounds.VolumeSafe())
                {
                    listA = ScaleStandardizedPoints(listA, aBounds, bBounds);
                }
                else
                {
                    listB = ScaleStandardizedPoints(listB, bBounds, aBounds);
                }

                break;
            case ProportionMethod.SmallestToBiggest:
                if (aBounds.VolumeSafe() < bBounds.VolumeSafe())
                {
                    listA = ScaleStandardizedPoints(listA, aBounds, bBounds);
                }
                else
                {
                    listB = ScaleStandardizedPoints(listB, bBounds, aBounds);
                }

                break;
            case ProportionMethod.None:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(proportionFixMethod), proportionFixMethod, null);
        }
    }

    private static List<Vector3> ScaleStandardizedPoints(IEnumerable<Vector3> source, Bounds sourceBounds, Bounds targetBounds)
    {
        var proportion = Bounds.ProportionalVector(sourceBounds, targetBounds);
        
        var scaleStandardizedPoints = source.Select(vector3 => Vector3.Scale(vector3, proportion)).ToList();
        return scaleStandardizedPoints;
    }
    
    private static float[] GetVectorsAxis(IReadOnlyList<Vector3> input, Axis axis)
    {
        float[] ret = new float[input.Count];
        for (int i = 0; i < input.Count; i++)
        {
            switch (axis)
            {
                case Axis.X:
                    ret[i] = input[i].x;
                    break;
                case Axis.Y:
                    ret[i] = input[i].y;
                    break;
                case Axis.Z:
                    ret[i] = input[i].z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
            }
        }
        return ret;
    }
    
    private static List<Vector3> StandardizeVectors(IReadOnlyCollection<Vector3> input)
    {
        //Get the center of mass of the drawing
        var middlePoint = input.Aggregate(Vector3.zero, 
            (current, vector3) => current + vector3);
        middlePoint /= input.Count;
        
        //Store each vector in the list as an offset to that center
        return input.Select(vector3 => middlePoint - vector3).ToList();
    }
}