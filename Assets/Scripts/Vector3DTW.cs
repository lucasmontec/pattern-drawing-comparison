using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Vector3Dtw
{
    public enum ProportionMethod
    {
        BiggestToSmallest,
        SmallestToBiggest,
        None
    }
    
    public enum DtwTrimMethod
    {
        TrimFirst,
        TrimSecond,
        Auto,
        None
    }

    private enum Axis
    {
        X,
        Y,
        Z
    }
    
    public static float Compute(IEnumerable<Vector3> first, IReadOnlyList<Vector3> second, DtwTrimMethod trim = DtwTrimMethod.Auto, ProportionMethod proportionFixMethod = ProportionMethod.SmallestToBiggest)
    {
        var firstList = first.ToList();
        var listA = new List<Vector3>(firstList);
        var listB = new List<Vector3>(second);

        //Trim lists
        switch (trim)
        {
            case DtwTrimMethod.TrimFirst:
                if (firstList.Count > second.Count)
                {
                    listA = listA.GetRange(0, listB.Count);
                }
                break;
            case DtwTrimMethod.TrimSecond:
                if (second.Count > firstList.Count)
                {
                    listB = listB.GetRange(0, listA.Count);
                }
                break;
            case DtwTrimMethod.Auto:
                if (listA.Count > listB.Count)
                {
                    listA = listA.GetRange(0, listB.Count);
                }
                else
                {
                    listB = listB.GetRange(0, listA.Count);
                }
                break;
            case DtwTrimMethod.None:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(trim), trim, null);
        }
        
        //Standardize vectors of both lists (align and transform them to offsets from their center of mass)
        listA = StandardizeVectors(listA);
        listB = StandardizeVectors(listB);

        //Fix proportion between the drawings
        NormalizeProportion(proportionFixMethod, ref listA, ref listB);

        var xA = GetVectorsAxis(listA, Axis.X);
        var xB = GetVectorsAxis(listB, Axis.X);
        
        var yA = GetVectorsAxis(listA, Axis.X);
        var yB = GetVectorsAxis(listB, Axis.X);
        
        var zA = GetVectorsAxis(listA, Axis.X);
        var zB = GetVectorsAxis(listB, Axis.X);
        
        var dtwX = new SimpleDtw(xA, xB);
        var dtwY = new SimpleDtw(yA, yB);
        var dtwZ = new SimpleDtw(zA, zB);
        
        dtwX.ComputeDtw();
        dtwY.ComputeDtw();
        dtwZ.ComputeDtw();

        return (dtwX.GetSum() + dtwY.GetSum() + dtwZ.GetSum()) / 3f;
    }

    private static void NormalizeProportion(ProportionMethod proportionFixMethod, ref List<Vector3> listA, ref List<Vector3> listB)
    {
        Bounds aBounds = Bounds.FromPoints(listA);
        Bounds bBounds = Bounds.FromPoints(listB);
        switch (proportionFixMethod)
        {
            case ProportionMethod.BiggestToSmallest:
                if (aBounds.VolumeSafe() > bBounds.VolumeSafe())
                {
                    listA = ScaleStandardizedPoints(listA, aBounds, bBounds);
                }
                else
                {
                    listB = ScaleStandardizedPoints(listB, bBounds, aBounds);
                }

                break;
            case ProportionMethod.SmallestToBiggest:
                if (aBounds.VolumeSafe() < bBounds.VolumeSafe())
                {
                    listA = ScaleStandardizedPoints(listA, aBounds, bBounds);
                }
                else
                {
                    listB = ScaleStandardizedPoints(listB, bBounds, aBounds);
                }

                break;
            case ProportionMethod.None:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(proportionFixMethod), proportionFixMethod, null);
        }
    }

    private static List<Vector3> ScaleStandardizedPoints(IEnumerable<Vector3> source, Bounds sourceBounds, Bounds targetBounds)
    {
        var proportion = Bounds.ProportionalVector(sourceBounds, targetBounds);
        var scaleStandardizedPoints = source.Select(vector3 => Vector3.Scale(vector3, proportion)).ToList();
        return scaleStandardizedPoints;
    }
    
    private static float[] GetVectorsAxis(IReadOnlyList<Vector3> input, Axis axis)
    {
        float[] ret = new float[input.Count];
        for (int i = 0; i < input.Count; i++)
        {
            switch (axis)
            {
                case Axis.X:
                    ret[i] = input[i].x;
                    break;
                case Axis.Y:
                    ret[i] = input[i].y;
                    break;
                case Axis.Z:
                    ret[i] = input[i].z;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
            }
        }
        return ret;
    }
    
    private static List<Vector3> StandardizeVectors(IReadOnlyCollection<Vector3> input)
    {
        //Get the center of mass of the drawing
        var middlePoint = input.Aggregate(Vector3.zero, 
            (current, vector3) => current + vector3);
        middlePoint /= input.Count;
        
        //Store each vector in the list as an offset to that center
        return input.Select(vector3 => middlePoint - vector3).ToList();
    }
}