﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public class LineDraw : SerializedMonoBehaviour
{
    public LineRenderer lineRenderer;

    public float resolutionDistance = 1f;

    [OdinSerialize]
    public bool CanDraw { get; set; }
    
    public bool drawOnce;

    [ShowIf(nameof(drawOnce))]
    public UnityEvent onLineReady;
    
    private Vector3 lastPoint;

    private readonly List<Vector3> points = new List<Vector3>();

    public ReadOnlyCollection<Vector3> Points => points.AsReadOnly();
    
    private bool lineChanged;

    private bool lineReady;

    public void ResetLine()
    {
        lineReady = false;
        CanDraw = true;
    }
    
    private void Update()
    {
        if(!CanDraw) return;
        if(lineReady) return;
        
        var currentPoint =  Camera.main?.ScreenToWorldPoint(Input.mousePosition) ?? Vector3.zero;
        currentPoint.z = 0;

        if (drawOnce && Input.GetMouseButtonUp(0))
        {
            lineReady = true;
            CanDraw = false;

            Observable.NextFrame().Subscribe(_ =>
            {
                onLineReady?.Invoke();
            });
        }
        
        //Start of the line
        if (Input.GetMouseButtonDown(0))
        {
            points.Clear();
            points.Add(currentPoint);
            lineChanged = true;
            
            lastPoint = currentPoint;
        }
        
        //Drag
        if (Input.GetMouseButton(0))
        {
            //Check distance
            if (Vector3.Distance(lastPoint, currentPoint) > resolutionDistance)
            {
                lastPoint = currentPoint;
                points.Add(currentPoint);
                lineChanged = true;
            }
        }

        if (lineChanged)
        {
            lineChanged = false;
            lineRenderer.positionCount = points.Count;
            lineRenderer.SetPositions(points.ToArray());
        }
    }
}
