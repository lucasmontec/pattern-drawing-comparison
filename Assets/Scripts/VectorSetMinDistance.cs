using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using static Unity.Mathematics.math;

public class VectorSetMinDistance
{
    private readonly Dictionary<List<Vector3>, VectorGrid> grids = new Dictionary<List<Vector3>, VectorGrid>();

    public float MinDistance(Vector3 point, List<Vector3> points, float division)
    {
        VectorGrid vectorGrid;
        if (!grids.ContainsKey(points))
        {
            vectorGrid = new VectorGrid(division);
            vectorGrid.AddToGrid(points);
            grids[points] = vectorGrid;
        }

        vectorGrid = grids[points];

        var nearestSet = vectorGrid.FindNearestSet(point);

        var pointInSets = nearestSet.ToList();
        if (!pointInSets.Any())
        {
            return -1f;
        }
        
        float minDistance = float.MaxValue;
        foreach (var pointInSet in pointInSets)
        {
            var distance = distancesq(pointInSet, point);
            if (distance < minDistance)
            {
                minDistance = distance;
            }
        }

        return minDistance;
    }
}