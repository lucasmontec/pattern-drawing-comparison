﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace EditModeTests
{
    public class TestNearestPoints
    {
        private static List<Vector3> GetTestShape()
        {
            return new List<Vector3>
            {
                new Vector3(3.157147f, -1.54807f, 0),
                new Vector3(3.283978f, 1.713007f, 0),
                new Vector3(-1.481261f, 2.390516f, 0),
                new Vector3(-2.822051f, -2.026843f, 0),
                new Vector3(1.399625f, -2.180412f, 0)
            };
        }

        private static List<Vector3> GetTestSquare()
        {
            return new List<Vector3>
            {
                new Vector3(-1, 1, 0),
                new Vector3(-1, -1, 0),
                new Vector3(1, -1, 0),
                new Vector3(1, 1, 0)
            };
        }
        
        private void DrawShape(IReadOnlyList<Vector3> shape, Vector3 source, Vector3 nearest)
        {
            for (var index = 0; index < shape.Count-1; index++)
            {
                var point = shape[index];
                var nextPoint = shape[index+1];

                Debug.DrawLine(-point, -nextPoint, Color.blue, 5f);
            }
            
            Debug.DrawLine(-source, -nearest, Color.green, 5f);
        }

        [Test]
        public void TestBugCase()
        {
            Vector3 source = new Vector3(-0.411245f, -2.245664f, 0);

            var predictedNearest = new Vector3(1.399625f, -2.180412f, 0);

            VectorGrid targetGrid = new VectorGrid(0.5f);
            var testShape = GetTestShape();
            targetGrid.AddToGrid(testShape);

            var nearest = (Vector3) targetGrid.FindNearest(source);

            Debug.Log($"source {source} nearest {nearest} {Vector3.Distance(source, nearest)} " +
                      $"predicted {predictedNearest} {Vector3.Distance(source, predictedNearest)}");
            
            DrawShape(testShape, source, nearest);
            Assert.True(nearest == predictedNearest);
        }

        [Test]
        public void TestNearestBottomLeft3()
        {
            Vector3 source = new Vector3(-0.2f, -0.8f, 0);

            var predictedNearest = new Vector3(-1, -1, 0);

            VectorGrid targetGrid = new VectorGrid(0.4f);
            targetGrid.AddToGrid(GetTestSquare());

            var nearest = (Vector3) targetGrid.FindNearest(source);

            Assert.True(nearest == predictedNearest);
        }

        [Test]
        public void TestNearestBottomLeft2()
        {
            Vector3 source = new Vector3(-0.8f, -0.2f, 0);

            var predictedNearest = new Vector3(-1, -1, 0);

            VectorGrid targetGrid = new VectorGrid(0.4f);
            targetGrid.AddToGrid(GetTestSquare());

            var nearest = (Vector3) targetGrid.FindNearest(source);

            Assert.True(nearest == predictedNearest);
        }

        [Test]
        public void TestNearestBottomLeft()
        {
            Vector3 source = new Vector3(-1.5f, -1.2f, 0);

            var predictedNearest = new Vector3(-1, -1, 0);

            VectorGrid targetGrid = new VectorGrid(0.4f);
            targetGrid.AddToGrid(GetTestSquare());

            var nearest = (Vector3) targetGrid.FindNearest(source);

            Assert.True(nearest == predictedNearest);
        }

        [Test]
        public void TestNearestTopLeft()
        {
            Vector3 source = new Vector3(-1.5f, 1.2f, 0);

            var predictedNearest = new Vector3(-1, 1, 0);

            VectorGrid targetGrid = new VectorGrid(0.4f);
            targetGrid.AddToGrid(GetTestSquare());

            var nearest = (Vector3) targetGrid.FindNearest(source);

            Assert.True(nearest == predictedNearest);
        }
    }
}